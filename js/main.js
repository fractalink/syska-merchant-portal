/*
  @Author : Fractalink Design Studio
  @Project : Syska Mart Dashboard Website
  @Dev : Sukanya Pedamkar
  @Date : 1st March 2016;
*/
/*---------------------------------------------------------------------------------*/
var genericObj = {};


//SideSection Slide Js
genericObj.sideBarSlideSFunc = {
    init: function() {
        $(".menu-button").on('click', function() {
            if ($(this).hasClass("active") == true) {
                $(".mainWrapper").removeClass("menu-collapsed");
                $(this).removeClass("active");
                $(".sidebarSection .sidebar-text,.sidebarSection .nav-arrow,.user-info-wrap .user-info").hide();

                setTimeout(
                    function() {
                        $(".sidebarSection .sidebar-text,.sidebarSection .nav-arrow,.user-info-wrap .user-info").fadeIn(500);
                    }, 300);
            } else {
                $(".mainWrapper").addClass("menu-collapsed");
                $(this).addClass("active");
                $(".sidebarSection .sidebar-text,.sidebarSection .nav-arrow,.user-info-wrap .user-info").hide();
            }

        });
    }
}

//SideSection Collapse Js
genericObj.sideBarCollapseFunc = {
    init: function() {
        $(".sidebar-nav li .sidebar-nav-item").on('click', function() {
            if ($(".mainWrapper").hasClass("menu-collapsed") == true) {
                $(".sidebar-nav li").removeClass('active');
                $(this).parent().addClass("active");
            } else if ($(".mainWrapper").hasClass("menu-collapsed") == false) {
                if ($(this).parent().hasClass("active") == true) {
                    $(this).find(".nav-arrow").removeClass('icon-down-arrow').addClass('icon-right-arrow');
                    $(".sidebar-nav li .subMenu li").find(".nav-arrow").removeClass('icon-right-arrow').addClass('icon-down-arrow');
                } else {
                    $(".sidebar-nav li").removeClass('active');
                    $(this).parent().addClass('active');
                    $(".sidebar-nav > li").find(".nav-arrow").removeClass('icon-down-arrow').addClass('icon-right-arrow');
                    $(this).parent().find(".nav-arrow").removeClass('icon-right-arrow').addClass('icon-down-arrow');
                    $(".sidebar-nav li .subMenu li").find(".nav-arrow").removeClass('icon-down-arrow').addClass('icon-right-arrow');
                }
            }
        });
    }
}

//Rating Graph Js
genericObj.customizeBarGraph = {
    init: function(chartObj) {
        new Chartist.Bar(chartObj, {
            labels: ['1 star', '2 star', '3 star', '4 star', '5 star'],
            series: [
                [3, 1, 1, 6, 7]
            ]
        }, {
            horizontalBars: true,
            responsive: true,
            axisX: {
                showLabel: false,
                showGrid: false
            },
            axisY: {
                offset: 50,
                showGrid: false
            }
        });
    }
}

// SetPrice JS
genericObj.setPriceFunc = {
    init: function() {
        $(".setprice-wrap .link").on("click", function() {
            $(this).parents(".edit-tab").find(".input-wrap").show();
            $(this).hide();
        })
    },
    checkBoxclick: function() {
        $('.mod-product-edit-list input[type=checkbox]').click(function() {
            if ($(this).prop("checked") == true) {
                $(".mod-product-edit-list table tr").removeClass("active")
                $(this).parents("tr").addClass("active");
            } else if ($(this).prop("checked") == false) {
                $(this).parents("tr").removeClass("active");
            }
            genericObj.setPriceFunc.qtyAppendFunc();

        });
    },
    qtyAppendFunc: function() {
        $(".qty-wrap").each(function() {
            var val = $(this).find("input[type=text]").val();
            $(this).find(".qty-val").text(val)
        })
    },
    setValFunc: function() {
        $(".edit-val .icon").on("click", function() {
            if ($(this).parent().hasClass("active") == true) {
                $(this).parent().removeClass("active")
                var inputVal = $(this).parent().find(".value-wrap input[type=text]").val();
                $(this).parent().find(".value").text(inputVal)
            } else if ($(this).parent().hasClass("active") == false) {
                $(".edit-val").removeClass("active")
                $(this).parent().addClass("active")
                var inputVal = $(this).parent().find(".value-wrap input[type=text]").val();
                $(this).parent().find(".value").text(inputVal)
            }
        })
    }

}

/* Select Js */
genericObj.customDropdown = {
    init: function(obj, opts){
        $(obj).select2(opts)
        genericObj.dropdownEditFunc.init();
    }
}

/* Dropdown Edit Function Js */
genericObj.dropdownEditFunc = {
    init: function() {

        $(".select2-chosen").each(function(e) {
            if ($($(this).text() == "Active")) {
                $(this).parents(".status-selectBox").addClass("green-text");
            } else if ($($(this).text() == "Inactive")) {
                $(this).parents(".status-selectBox").addClass("red-text");

            }
        })
        $(".status-selectBox").on("change", function(e) {
            if (e.val == "Active") {
                $(this).removeClass("red-text").addClass("green-text")
            } else if (e.val == "Inactive") {
                $(this).removeClass("green-text").addClass("red-text")
            }
        })
    }
}

 // upload Image File JS
genericObj.openBrowseWindow = {
    init: function() {
        $(".browseWindow .img-upload").on("click", function() {
            var tt = $("<input type='file' />");
            tt.trigger("click");
            return false;
        })
    }
}

// Date Time Picker JS
genericObj.dateTimePickerFunc = {
    init: function(startObj, endObj) {
        $(startObj).datetimepicker({
            weekStart: 1,
            todayBtn: 0,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            pickerPosition: "bottom-left"
        });

        $(endObj).datetimepicker({
            weekStart: 1,
            todayBtn: 0,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            pickerPosition: "bottom-left"
        });

        var cDate = new Date();
        var cM = cDate.getMonth();
        var cD = cDate.getDate();
        var cY = cDate.getFullYear();
        var endDate = new Date()
        var newDate = endDate.setDate(cD + 7);
        var eM = endDate.getMonth()
        var eD = endDate.getDate()
        var eY = endDate.getFullYear()

        $(startObj).datetimepicker('update', "'" + cD + "/" + (cM + 1) + "/" + cY + "'");
        $(endObj).datetimepicker('update', "'" + eD + "/" + (eM + 1) + "/" + eY + "'");


        $(startObj)
            .datetimepicker()
            .on('changeDate', function(ev) {

                $(endObj).datetimepicker('setStartDate', ev.date);

            });

        $(endObj)
            .datetimepicker()
            .on('changeDate', function(ev) {

                $(startObj).datetimepicker('setEndDate', ev.date);

            });
    }
}

// Tooltipster Js
genericObj.tooltipFunc = {
    init: function(obj, option) {
        $(obj).tooltipster(option)
    }
}

// RangeSlider Js
 genericObj.valueSlider = {
    init:function(rObj, rOpt){
        $(rObj).each(function(){
            var thisObj = this;
            noUiSlider.create(thisObj,rOpt)
    });
    }
 }



$(document).ready(function() {

    $("a").each(function() {
        if ($(this).attr('href') == '#' || $(this).attr('href') == ' ') {
            $(this).attr('href', 'javascript:void(0)');
        }
    });

    
    $('input, textarea').placeholder();

    /*-------------------------------- Header Js -----------------------------*/
    //SideSection Collapse Js
    if ($(".sidebar-nav").length != 0) {
        genericObj.sideBarCollapseFunc.init();
    }

    //SideSection Slide Js
    if ($(".menu-button").length != 0) {

        genericObj.sideBarSlideSFunc.init();
    }

    //Rating Graph Js
    if ($(".rate-chart").length != 0) {
        genericObj.customizeBarGraph.init(".rate-chart");
    }


    /*------------------------------ Custom Scollbar Js ------------------------*/
    //Custom Scrollbar Js
    if ($(".mod-select-listing").length != 0) {
        $(".mod-select-listing ul").mCustomScrollbar();
    }

    //Custom Scrollbar Js
    if ($(".mod-product-edit-list").length != 0) {
        $(".customScroll").mCustomScrollbar();
    }

    //Custom Scrollbar Js
    if ($(".mod-product-spec-list").length != 0) {
        $(".customScroll").mCustomScrollbar();
    }



    /*------------------------------ Select Box Js ------------------------*/
    // Selectbox JS
    if ($(".custom-selectBox").length != 0) {
        var options = {
            containerCssClass : "custom-selectBox",
            dropdownCssClass : "custom-selectBox-dd",
            minimumResultsForSearch: -1
        }
        genericObj.customDropdown.init(".custom-selectBox",options)
    }

    // Selectbox JS
    if ($(".select-noborder").length != 0) {
        var options = {
            containerCssClass : "select-noborder",
            dropdownCssClass : "custom-selectBox-dd",
            minimumResultsForSearch: -1
        }
        genericObj.customDropdown.init(".select-noborder", options)
    }

    // Selectbox JS
    if ($(".status-selectBox").length != 0) {
        var options = {
            containerCssClass : "status-selectBox",
            dropdownCssClass : "custom-selectBox-dd",
            minimumResultsForSearch: -1  

        }
        genericObj.customDropdown.init(".status-selectBox", options)



    }

    // Selectbox JS
    if ($(".select-with-border").length != 0) {
        var options = {
            containerCssClass : "select-with-border",
            dropdownCssClass : "custom-selectBox-dd",
            minimumResultsForSearch: -1
        }
        genericObj.customDropdown.init(".select-with-border", options)
    }

    /*------------------------------ Edit Tab Js ------------------------*/
    // SetPrice JS
    if ($(".setprice-wrap").length != 0) {
        genericObj.setPriceFunc.init();
    }

    // SetPrice JS
    if ($(".checkbox-wrap,.qty-wrap").length != 0) {
        genericObj.setPriceFunc.checkBoxclick();
    }

    // SetPrice JS
    if ($(".checkbox-wrap,.qty-wrap").length != 0) {
        genericObj.setPriceFunc.qtyAppendFunc();
    }
    // SetPrice JS
    if ($(".edit-val").length != 0) {
        genericObj.setPriceFunc.setValFunc();
    }


    /*------------------------------ Common Js ------------------------*/

    // uploadFile JS
    if ($("#uploadBtn").length != 0) {
        $("#uploadBtn").on('change', function() {
            $("#uploadFile").val($("#uploadBtn").val());
        })
    }

    // Date Time Picker JS
    if ($("#startDate").length != 0) {
        genericObj.dateTimePickerFunc.init("#startDate", "#endDate")
    }

    // upload Image File JS
    if ($(".img-upload").length != 0) {
        genericObj.openBrowseWindow.init(".browseWindow .img-upload");
    }

    // Tooltipster Js
    if ($(".tooltip-priceLink").length != 0) {
        var opt = {
            functionInit: function() {
                return $(this).parents(".tooltip-wrap").find(".tooltip-cont").html()
            },
            theme: 'theme1',
            maxWidth: 200,
            contentAsHTML: true,
            positionTracker: true,
            position: 'bottom',
            onlyOne: true,
            arrow: true,
            minWidth: 150
        }
        genericObj.tooltipFunc.init(".tooltip-priceLink", opt)
    }

    // RangeSlider Js
    if ($(".rangeSlider1").length != 0) {
        var opt = {
            start: [4000000],
            tooltips: [wNumb({prefix:'&#8377;',thousand:',',decimals:0})],
            range: {
                'min': 0,
                'max': 9000000
            }
        }
        genericObj.valueSlider.init(".rangeSlider1",opt)
    }

})
